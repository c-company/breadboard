// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.geometry.Translation3d;
import edu.wpi.first.math.util.Units;
import swervelib.math.Matter;


/**
 * The Constants class provides a convenient place for teams to hold robot-wide numerical or boolean
 * constants. This class should not be used for any other purpose. All constants should be declared
 * globally (i.e. public static). Do not put anything functional in this class.
 *
 * <p>It is advised to statically import this class (or one of its inner classes) wherever the
 * constants are needed, to reduce verbosity.
 */
public final class Constants {


/*** Drivebase Constants ***/

  public static final double ROBOT_MASS = (50 - 20.3) * 0.453592; // 32lbs * kg per pound
  public static final Matter CHASSIS    = new Matter(new Translation3d(0, 0, Units.inchesToMeters(8)), ROBOT_MASS);
  public static final double LOOP_TIME  = 0.13; //s, 20ms + 110ms sprk max velocity lag
  public static final double MAX_SPEED  = Units.feetToMeters(14.5);

    // Hold time on motor brakes when disabled
    public static final double WHEEL_LOCK_TIME = 10; // seconds
  
    //Set slew limits
    public static final double kTranslationLimit = 0; //updated per jason on 2/10/24, prev value 1
    public static final double kStrafeLimit = 0; //updated per jason on 2/10/24, prev value 1
    public static final double kRotationLimit = 0; //updated per jason on 2/10/24, prev value 1

  
/*** Limelight Constants ***/
    public static final String kLimelightName = "limelight-bb";
    public static final int kAllAprilTagPipeline = 0;

    public static final double kAPRILTAG_X_TOLERANCE = 0.05;
    public static final double kAPRILTAG_Y_TOLERANCE = 0.05;
    public static final double kAPRILTAG_ROTATION_TOLERANCE = .025; // Radians

    // Field Element locations in LL Coordinates
    public static final double kRedAmpX = 2.0;
    public static final double kRedAmpY = 0.0;
    public static final double kRedAmpRot = 90;
  

/*** Operator Constants ***/

    // Joystick Deadband
    public static final double DEADBAND        = 0.1;
    public static final double LEFT_X_DEADBAND = 0.01;
    public static final double LEFT_Y_DEADBAND = 0.01;
    public static final double RIGHT_X_DEADBAND = 0.01;
    public static final double TURN_CONSTANT = 6; 
    public static final int kDriverControllerPort = 0;

/*** Robot Pose Constants ***/

public static final Pose2d kLeftBlueCoralStation =
    null;

public static final Pose2d kRightBlueCoralStation =
    null;

public static final Pose2d kBlueProcessor =
    null;

    public static final Pose2d kLeftRedCoralStation =
    null;

public static final Pose2d kRightRedCoralStation =
    null;

public static final Pose2d kRedProcessor =
    null;


public static final Pose2d[] kReefPositionsLeft = {
/* 0 */  null, //0
/* 1 */  null, //1
/* 2 */  null, //2
/* 3 */  null, //3
/* 4 */  null, //4
/* 5 */  null, //5
/* 6 */  null, //6
/* 7 */  null, //7
/* 8 */  null, //8
/* 9 */  null, //9
/* 10 */  null, //10
/* 11 */  null, //11
/* 12 */  null, //12
/* 13 */  null, //13
/* 14 */  null, //14
/* 15 */  null, //15
/* 16 */  null, //16
/* 17 */  null, //17                                                                                 
/* 18 */  new Pose2d(new Translation2d(3.3, 4), new Rotation2d(Math.toRadians(0))), //18
/* 19 */  null, //19
/* 20 */  null, //20
/* 21 */  null, //21
/* 22 */  null //22
};

public static final Pose2d[] kReefPositionsRight = {
  /* 0 */  null, //0
  /* 1 */  null, //1
  /* 2 */  null, //2
  /* 3 */  null, //3
  /* 4 */  null, //4
  /* 5 */  null, //5
  /* 6 */  null, //6
  /* 7 */  null, //7
  /* 8 */  null, //8
  /* 9 */  null, //9
  /* 10 */  null, //10
  /* 11 */  null, //11
  /* 12 */  null, //12
  /* 13 */  null, //13
  /* 14 */  null, //14
  /* 15 */  null, //15
  /* 16 */  null, //16
  /* 17 */  null, //17                                                                                 
  /* 18 */  null, //18
  /* 19 */  null, //19
  /* 20 */  null, //20
  /* 21 */  null, //21
  /* 22 */  null  //22
  };
}
