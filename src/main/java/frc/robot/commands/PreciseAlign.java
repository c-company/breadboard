// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.Command;

import java.util.function.Supplier;


import edu.wpi.first.math.controller.ProfiledPIDController;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Pose3d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Rotation3d;
import edu.wpi.first.math.geometry.Transform3d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.geometry.Translation3d;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.math.trajectory.TrapezoidProfile;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.Constants;
import frc.robot.subsystems.LimelightHelpers;
import frc.robot.subsystems.SwerveSubsystem;


public class PreciseAlign extends Command {
  /** Creates a new PreciseAlign. */
  private static final TrapezoidProfile.Constraints X_CONSTRAINTS = new TrapezoidProfile.Constraints(3, 2);
  private static final TrapezoidProfile.Constraints Y_CONSTRAINTS = new TrapezoidProfile.Constraints(3, 2);
  private static final TrapezoidProfile.Constraints OMEGA_CONSTRAINTS =   new TrapezoidProfile.Constraints(8, 8);

  private SwerveSubsystem m_swerve;
  private Pose2d m_targetPose;

  private final ProfiledPIDController xController = new ProfiledPIDController(5, 0, 0, X_CONSTRAINTS);
  private final ProfiledPIDController yController = new ProfiledPIDController(5, 0, 0, Y_CONSTRAINTS);
  private final ProfiledPIDController omegaController = new ProfiledPIDController(5, 0, 0, OMEGA_CONSTRAINTS);

  public PreciseAlign(SwerveSubsystem p_swerve) {
    m_swerve = p_swerve;
    
    xController.setTolerance(0.05);
    yController.setTolerance(0.05);
    omegaController.setTolerance(Units.degreesToRadians(1));
    omegaController.enableContinuousInput(-Math.PI, Math.PI);

    addRequirements(p_swerve);
   
  }


  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    m_targetPose = GetTagPose();
    xController.setGoal(m_targetPose.getX());
    yController.setGoal(m_targetPose.getY());
    omegaController.setGoal(m_targetPose.getRotation().getRadians());
    omegaController.reset(m_swerve.getPose().getRotation().getRadians());
    xController.reset(m_swerve.getPose().getX());
    yController.reset(m_swerve.getPose().getY());

     SmartDashboard.putNumber("PoseX", m_targetPose.getX());
    SmartDashboard.putNumber("PoseY", m_targetPose.getY());
    SmartDashboard.putNumber("PoseRot", m_targetPose.getRotation().getDegrees());

  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {

    var xSpeed = xController.calculate(m_swerve.getPose().getX());
    if (xController.atGoal()) {
      xSpeed = 0;
    }

    var ySpeed = yController.calculate(m_swerve.getPose().getY());
    if (yController.atGoal()) {
      ySpeed = 0;
    }

    var omegaSpeed = omegaController.calculate(m_swerve.getPose().getRotation().getRadians());
    if (omegaController.atGoal()) {
      omegaSpeed = 0;
    }

    m_swerve.drive(
      ChassisSpeeds.fromFieldRelativeSpeeds(xSpeed, ySpeed, omegaSpeed, m_swerve.getPose().getRotation()));
    

  }

  

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    m_swerve.setMotorBrake(true);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    if (xController.atGoal() && yController.atGoal() && omegaController.atGoal()) {
      return true;
    } else { 
      return false;
    }
  }

   // Lookup pose for AprilTag
  public Pose2d GetTagPose(){

    double TagNum = LimelightHelpers.getFiducialID(Constants.kLimelightName);
    SmartDashboard.putNumber("TagNum", TagNum);
    SmartDashboard.putNumber("TargetCount", LimelightHelpers.getTargetCount(Constants.kLimelightName));
  

    if(LimelightHelpers.getTargetCount(Constants.kLimelightName) > 0){
      //get primary Apriltag from Limelight
      
            
      // Lookup Pose for AT number
      if(Constants.kReefPositionsLeft[(int)TagNum] != null){
        return Constants.kReefPositionsLeft[(int)TagNum];
        //return new Pose2d(new Translation2d(3.3, 4), new Rotation2d(Math.toRadians(0)));
      }
    }
    
    // Return current pose if no targets found or April Tag has undefined pose
    return m_swerve.getPose();

  }
}
