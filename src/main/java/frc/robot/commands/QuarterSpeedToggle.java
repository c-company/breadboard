// package frc.robot.commands;

// import edu.wpi.first.math.MathUtil;
// import edu.wpi.first.wpilibj2.command.Command;
// import edu.wpi.first.wpilibj2.command.Commands;
// import edu.wpi.first.wpilibj2.command.InstantCommand;
// import edu.wpi.first.wpilibj2.command.button.CommandXboxController;
// import frc.robot.Constants.OperatorConstants;
// import frc.robot.subsystems.SwerveSubsystem;

// public class QuarterSpeed extends Command {
//     private final SwerveSubsystem m_SwerveSubsystem;
//     private final CommandXboxController m_driverXbox;

//     public QuarterSpeed(SwerveSubsystem p_SwerveSubsystem, CommandXboxController p_driverXbox) {
//         m_SwerveSubsystem = p_SwerveSubsystem;
//         m_driverXbox = p_driverXbox;

//         addRequirements(p_SwerveSubsystem);
//     }

//     @Override 
//     public void initialize() {

//     }

//     @Override
//     public void execute() {
//         m_driverXbox.button(5).onTrue(m_SwerveSubsystem.runOnce(() -> m_SwerveSubsystem.setQuarterspeed(true)));
//         m_driverXbox.button(6).onTrue(m_SwerveSubsystem.runOnce(() -> m_SwerveSubsystem.setQuarterspeed(false)));
        

//         if (m_SwerveSubsystem.getQuarterspeed() == true) {
//             double speed = 1.5;
//              System.out.println(speed);
//             m_SwerveSubsystem.driveCommand(
// () -> MathUtil.applyDeadband(-(m_driverXbox.getLeftY() ), OperatorConstants.LEFT_Y_DEADBAND),
// () -> MathUtil.applyDeadband(-(m_driverXbox.getLeftX() ), OperatorConstants.LEFT_X_DEADBAND),
// () -> -(m_driverXbox.getRightX()));
//   } else {
//     m_SwerveSubsystem.driveCommand(
// () -> MathUtil.applyDeadband(-(m_driverXbox.getLeftY()), OperatorConstants.LEFT_Y_DEADBAND),
// () -> MathUtil.applyDeadband(-(m_driverXbox.getLeftX()), OperatorConstants.LEFT_X_DEADBAND),
// () -> -(m_driverXbox.getRightX() / -1.15));
//   }
//     }



// }